from random import random, randint, uniform
from math import sin, e
from numpy import exp
from random2 import xrange
 
INTERVALOS = (-1.0, 1.0)
 
 
def fitness(individuo):
    "Objetivo: acha o valor de x que maximiza y em: y = (-x**2 + 2*x - 3)"
 
    # Decodifica o cromossomo
    x = individuo[0]
    y = individuo[1]
    z = individuo[2]

    # maximo da formula 10(xy2)2+ 2sen(x) +zez+y2sen(y)

    return 10 * (x*y) * 2 + 2 * sin(x) + z * e ** z + y * 2 + sin(y) 

 
def individuo(tamanho, inf = INTERVALOS[0], sup = INTERVALOS[1]):
    'Define um cromossomo'
    return [uniform(inf, sup) for i in xrange(tamanho)]
 
 
def populacao(tam_populacao, tam_cromossomo):
    'Cria uma populacao de individuos'
    return [individuo(tam_cromossomo) for x in xrange(tam_populacao)]
 
 
def selecao_randomica(populacao):
    'Seleciona dois pais randomicamente'
    # tamanho da populacao
    tamanho = int(len(populacao) / 2)
 
    # lista de pais a ser retornada
    pais = []
 
    # Seleciona dois pais aleatorios e reserva para retornar
    for i in range(tamanho):
        pai1 = randint(0, tamanho - 1)
        pai2 = randint(0, tamanho - 1)
 
        pais.append((populacao[pai1], populacao[pai2]))
 
    return pais
 
def cruzamento_flat(pais, taxa_mutacao):
    'Realiza o cruzamento entre os pais - do tipo real'
 
    # lista de filhos
    filhos = []
 
    # percorre a lista de pais, cruzando dois a dois
    for x in pais:
        pai1 = x[0][1]
        pai2 = x[1][1]
 
        if (pai1[0] < pai2[0]):
            limite_inf = pai1[0]
            limite_sup = pai2[0]
        else:
            limite_inf = pai2[0]
            limite_sup = pai1[0]

 
        # Crossover (Recombinacao)
        filho1 = individuo(len(pai1), limite_inf, limite_sup)
        filho2 = individuo(len(pai1), limite_inf, limite_sup)
 
        # Mutacao
        if taxa_mutacao <= random():
            posicao = randint(0, len(filho1) - 1)
            filho1[posicao] = uniform(INTERVALOS[0], INTERVALOS[1])
 
        if taxa_mutacao <= random():
            posicao = randint(0, len(filho2) - 1)
            filho2[posicao] = uniform(INTERVALOS[0], INTERVALOS[1])
 
            filhos.append((fitness(filho1), filho1))
            filhos.append((fitness(filho2), filho2))
 
        return [x for x in sorted(filhos)]
 
def cruzamento(pais, taxa_mutacao):
    'Realiza o cruzamento entre os pais'
 
    # lista de filhos
    filhos = []
 
    # percorre a lista de pais, cruzando dois a dois
    for x in pais:
        pai1 = x[0][1]
        pai2 = x[1][1]
 
        # Seleciona o ponto de corte
        corte = randint(1, len(pai1) - 1)
 
        # Crossover (Recombinacao)
        filho1 = pai1[:corte] + pai2[corte:]
        filho2 = pai2[:corte] + pai1[corte:]
 
        # Mutacao
        if taxa_mutacao <= random():
            posicao = randint(0, len(filho1) - 1)
            filho1[posicao] = 1 if filho1[posicao] == 0 else 0
 
        if taxa_mutacao <= random():
            posicao = randint(0, len(filho2) - 1)
            filho2[posicao] = 1 if filho2[posicao] == 0 else 0
 
        filhos.append((fitness(filho1), filho1))
        filhos.append((fitness(filho2), filho2))
 
    return [x for x in sorted(filhos)]
 
 
def nova_geracao(pais, filhos):
    'Retorna os melhores de pais e filhos'
    lista = sorted(pais + filhos)
    return lista[:len(pais)]
 
 
# -----------------------------------------------
# PROGRAMA PRINCIPAL
# -----------------------------------------------
def main():
    # Parametros
    TAM_CROMOSSOMO = 3
    TAM_POPULACAO = 200
    print(e) 
    MAX_GERACOES = 500
    TAXA_MUTACAO = 0.05
 
    # 1. define a populacao inicial
    atual = populacao(TAM_POPULACAO, TAM_CROMOSSOMO)
    print(atual)
 
    # 2. calcula o fitness
    atual = [(fitness(x), x) for x in atual]
    atual = [x for x in sorted(atual)]
 
    # 3. criterio de parada
    geracao = 0
    while geracao < MAX_GERACOES:
 
        # 4. selecao dos pais
        pais = selecao_randomica(atual)
 
        # 5. crossover e mutacao
        filhos = cruzamento_flat(pais, TAXA_MUTACAO)
 
        # 6. selecao da geracao seguinte
        atual = nova_geracao(atual, filhos)
 
        print("GERACAO: " + str(geracao) + " y = " + str(atual[0]))
        #for i in atual: print(i)
 
        geracao += 1
    # print(atual[0])
 
 
if __name__ == "__main__":
    main()