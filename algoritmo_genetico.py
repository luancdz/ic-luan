#!/usr/local/bin/python3
 
from random import random, randint, choice
from random2 import xrange
from operator import add

TAM_CROMOSSOMO = 16
TAM_POPULACAO = 20
 
MAX_GERACOES = 20
TAXA_MUTACAO = 0.01
 
def converte_binario(individuo):
    x = 0
    pos = 0
    for i in range(7, -9, -1):
        x += individuo[pos] * 2 ** i
        pos += 1
     
    #x = 0
    #x += individuo[0] * 2 ** 3
    #x += individuo[1] * 2 ** 2
    #x += individuo[2] * 2 ** 1
    #x += individuo[3] * 2 ** 0
    #x += individuo[4] * 2 ** -1
    #x += individuo[5] * 2 ** -2
    #x += individuo[6] * 2 ** -3
    #x += individuo[7] * 2 ** -4    
     
    return x    
 
def fitness(individuo):
    "Objetivo: maximizar a qtde de 1's"
     
    # Decodifica o cromossomo
    x = converte_binario(individuo)
 
    # Calcula a funcao de avaliacao
    return - x ** 2 + 5 * x
 
def individuo(tamanho):
    'Define um cromossomo'
    return [ randint(0, 1) for i in xrange(tamanho) ]
 
def populacao(tam_populacao, tam_cromossomo):
    'Cria uma populacao de individuos'
    return [ individuo(tam_cromossomo) for x in xrange(tam_populacao) ]
 
def selecao_randomica(populacao):
    'Seleciona dois pais randomicamente'
    # tamanho da populacao
    tamanho = int(len(populacao) / 2)
     
    # lista de pais a ser retornada
    pais = []
 
    # Seleciona dois pais aleatorios e reserva para retornar
    for i in range(tamanho):
        pai1 = randint(0, tamanho - 1)
        pai2 = randint(0, tamanho - 1)
 
        pais.append((populacao[pai1], populacao[pai2]))
 
    return pais

def match(populacao):
    pai1 = randint(0, len(populacao) - 1)
    pai2 = randint(0, len(populacao) - 1)
    #print(populacao[pai1][0])
    if populacao[pai1][0] > populacao[pai2][0]:
        return pai1
    else:
        return pai2

def selecao_torneio(populacao):

    # tamanho da populacao
    tamanho = int(len(populacao) / 2)
    pais = []
    for i in range(tamanho):
        pai1 = match(populacao)
        pai2 = match(populacao)
 
        pais.append((populacao[pai1], populacao[pai2]))     

    return pais

def cruzamento_2_pontos(pais, taxa_mutacao):
    'Realiza o cruzamento entre os pais'
 
    # lista de filhos
    filhos = []
 
    # percorre a lista de pais, cruzando dois a dois
    for x in pais:
        pai1 = x[0][1]
        pai2 = x[1][1]
 
        # Seleciona o ponto de corte
        corte1 = 6
        corte2= 12
 
        # Crossover (Recombinacao)
        filho1 = pai1[:corte1] + pai2[corte1:]
        filho1 = filho1[:corte2] + pai1[corte2:]

        filho2 = pai2[:corte1] + pai1[corte1:]
        filho2 = filho2[:corte2] + pai2[corte2:]
 
        # Mutacao
        if taxa_mutacao <= random():
            posicao = randint(0, len(filho1) - 1)
            filho1[posicao] = 1 if filho1[posicao] == 0 else 0
            #if filho1[posicao] == 0: 
            #   filho1[posicao] = 1
            #else:
            #   filho1[posicao] = 0
 
        if taxa_mutacao <= random():
            posicao = randint(0, len(filho2) - 1)
            filho2[posicao] = 1 if filho2[posicao] == 0 else 0
            #if filho2[posicao] == 0: 
            #   filho2[posicao] = 1
            #else:
            #   filho2[posicao] = 0
 
 
        filhos.append((fitness(filho1), filho1))
        filhos.append((fitness(filho2), filho2))
 
    return [ x for x in sorted(filhos) ] 


def cruzamento_2_pontos(pais, taxa_mutacao):
    'Realiza o cruzamento entre os pais'
 
    # lista de filhos
    filhos = []
 
    # percorre a lista de pais, cruzando dois a dois
    for x in pais:
        pai1 = x[0][1]
        pai2 = x[1][1]
 
        # Seleciona o ponto de corte
        corte1 = 6
        corte2= 12
 
        # Crossover (Recombinacao)
        filho1 = pai1[:corte1] + pai2[corte1:]
        filho1 = filho1[:corte2] + pai1[corte2:]

        filho2 = pai2[:corte1] + pai1[corte1:]
        filho2 = filho2[:corte2] + pai2[corte2:]
 
        # Mutacao
        if taxa_mutacao <= random():
            posicao = randint(0, len(filho1) - 1)
            filho1[posicao] = 1 if filho1[posicao] == 0 else 0
            #if filho1[posicao] == 0: 
            #   filho1[posicao] = 1
            #else:
            #   filho1[posicao] = 0
 
        if taxa_mutacao <= random():
            posicao = randint(0, len(filho2) - 1)
            filho2[posicao] = 1 if filho2[posicao] == 0 else 0
            #if filho2[posicao] == 0: 
            #   filho2[posicao] = 1
            #else:
            #   filho2[posicao] = 0
 
 
        filhos.append((fitness(filho1), filho1))
        filhos.append((fitness(filho2), filho2))
 
    return [ x for x in sorted(filhos) ] 


def cruzamento_n_pontos(pais, taxa_mutacao):
    'Realiza o cruzamento entre os pais'
 
    # lista de filhos
    filhos = []
 
    # percorre a lista de pais, cruzando dois a dois
    for x in pais:
        pai1 = x[0][1]
        pai2 = x[1][1]
 
        # Seleciona o ponto de corte
        corte1 = 6
        corte2= 12
 
        # Crossover (Recombinacao)
        filho1 = pai1[:corte1] + pai2[corte1:]
        filho1 = filho1[:corte2] + pai1[corte2:]

        filho2 = pai2[:corte1] + pai1[corte1:]
        filho2 = filho2[:corte2] + pai2[corte2:]
 
        # Mutacao
        if taxa_mutacao <= random():
            posicao = randint(0, len(filho1) - 1)
            filho1[posicao] = 1 if filho1[posicao] == 0 else 0
            #if filho1[posicao] == 0: 
            #   filho1[posicao] = 1
            #else:
            #   filho1[posicao] = 0
 
        if taxa_mutacao <= random():
            posicao = randint(0, len(filho2) - 1)
            filho2[posicao] = 1 if filho2[posicao] == 0 else 0
            #if filho2[posicao] == 0: 
            #   filho2[posicao] = 1
            #else:
            #   filho2[posicao] = 0
 
 
        filhos.append((fitness(filho1), filho1))
        filhos.append((fitness(filho2), filho2))
 
    return [ x for x in sorted(filhos) ] 
 
 
def cruzamento(pais, taxa_mutacao):
    'Realiza o cruzamento entre os pais'
 
    # lista de filhos
    filhos = []
 
    # percorre a lista de pais, cruzando dois a dois
    for x in pais:
        pai1 = x[0][1]
        pai2 = x[1][1]
 
        # Seleciona o ponto de corte
        corte = randint(1, len(pai1) - 1)
 
        # Crossover (Recombinacao)
        filho1 = pai1[:corte] + pai2[corte:]
        filho2 = pai2[:corte] + pai1[corte:]
 
        # Mutacao
        if taxa_mutacao <= random():
            posicao = randint(0, len(filho1) - 1)
            filho1[posicao] = 1 if filho1[posicao] == 0 else 0
            #if filho1[posicao] == 0: 
            #   filho1[posicao] = 1
            #else:
            #   filho1[posicao] = 0
 
        if taxa_mutacao <= random():
            posicao = randint(0, len(filho2) - 1)
            filho2[posicao] = 1 if filho2[posicao] == 0 else 0
            #if filho2[posicao] == 0: 
            #   filho2[posicao] = 1
            #else:
            #   filho2[posicao] = 0
 
 
        filhos.append((fitness(filho1), filho1))
        filhos.append((fitness(filho2), filho2))
 
    return [ x for x in sorted(filhos) ] 
 
def nova_geracao(pais, filhos):
    'Retorna os melhores de pais e filhos'
    lista = sorted(pais + filhos, reverse=True)
    return lista[:len(pais)]
 
# -----------------------------------------------
# PROGRAMA PRINCIPAL
# -----------------------------------------------
def main():
 
    # 1. define a populacao inicial
    atual = populacao(TAM_POPULACAO, TAM_CROMOSSOMO)
    print(atual)
 
    # 2. calcula o fitness
    atual = [ (fitness(x), x) for x in atual ] 
    atual = [ x for x in sorted(atual, reverse=True) ] 
 
    # 3. criterio de parada
    geracao = 0
    while geracao < MAX_GERACOES:
 
        # 4. selecao dos pais
        pais = selecao_randomica(atual)
 
        # 5. crossover e mutacao
        filhos = cruzamento_2_pontos(pais, TAXA_MUTACAO)
 
        # 6. selecao da geracao seguinte
        atual = nova_geracao(atual, filhos)
        print(type(atual))
 
         
        print("GERACAO: " + str(geracao) + " y = " + str(atual[0])) 
        type(atual);
        print(converte_binario(atual[0][1]))
        geracao += 1
        #for i in atual: print(i)
        #print(atual[0])
 
 
if __name__ == "__main__":
    main()